openapi: 3.0.0
info:
  version: 0.0.1
  title: Delilah Resto
  description: Api para delivery de restaurant.

servers:
  # Added by API Auto Mocking Plugin
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/delilah-resto4/delilah-resto/0.0.1
  - description: Local Host
    url: http://localhost:5000

tags:
  - name: User
    description: Operaciones sobre los usuarios
  - name: Product
    description: Operaciones sobre los productos.
  - name: Order
    description: Operaciones sobre los pedidos.
  - name: PaymentMethod
    description: Operaciones sobre los medios de pagos.

paths:
  /register:
    post:
      tags:
        - User
      summary: crear usuario

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/userRegister'

      responses:
        200:
          description: el usuario fue creado.
        401:
          description: el email ya se encuentra registrado.

  /login:
    post:
      tags:
        - User
      summary: inicia sesión

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/userLogin'

      responses:
        200:
          description: sesion iniciada.
        401:
          description: error, los datos enviados no coinciden.

  /user:
    get:
      tags:
        - User
      summary: listar los usuarios

      parameters:
        - $ref: '#/components/parameters/userHeader'

      responses:
        200:
          description: lista de usuarios

  /user/{idUser}:
    get:
      tags:
        - User
      summary: usuario por ID

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/userPath'

      responses:
        200:
          description: muestra un usuarios por ID.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/userList'

    delete:
      tags:
        - User
      summary: Eliminacion logica de usuario por ID

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/userPath'

      responses:
        200:
          description: usuario desactivado.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/userList'

  /product:
    get:
      tags:
        - Product
      summary: listar los productos

      parameters:
        - $ref: '#/components/parameters/userHeader'

      responses:
        200:
          description: lista de productos activos
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/productsList'

    post:
      tags:
        - Product
      summary: crea un producto

      parameters:
        - $ref: '#/components/parameters/userHeader'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/productRegister'

      responses:
        200:
          description: crea un producto exitosamente.

  /product/{idProd}:
    get:
      tags:
        - Product
      summary: producto por ID

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/productPath'

      responses:
        200:
          description: muestra un producto por ID.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/productsList'

    put:
      tags:
        - Product
      summary: modifica producto por ID

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/productRegister'

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/productPath'

      responses:
        200:
          description: muestra un producto por modificado por ID.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/productsList'

    delete:
      tags:
        - Product
      summary: Eliminacion logica de un producto por ID

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/productPath'

      responses:
        200:
          description: se muestra un producto desactivado.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/productRegister'

  /order:
    get:
      tags:
        - Order
      summary: listar los pedidos

      parameters:
        - $ref: '#/components/parameters/userHeader'

      responses:
        200:
          description: lista los pedidos
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderList'

    post:
      tags:
        - Order
      summary: crea un pedido

      parameters:
        - $ref: '#/components/parameters/userHeader'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/orderRegister'

      responses:
        200:
          description: muestra un pedido realizado.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderRegister'

    put:
      tags:
        - Order
      summary: modifica la cantidad de un pedido

      parameters:
        - $ref: '#/components/parameters/userHeader'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/orderPutQty'

      responses:
        200:
          description: muestra un pedido con su cantidad modificada.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderList'

  /order/history:
    get:
      tags:
        - Order
      summary: muestra el historial de pedidos

      parameters:
        - $ref: '#/components/parameters/userHeader'

      responses:
        200:
          description: muestra el historia de pedidos de un usuario.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderListHistory'

  /order/{idOrder}:
    put:
      tags:
        - Order
      summary: cambia el estado de un pedido.
      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/orderPath'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/orderStatus'

      responses:
        200:
          description: muestra un pedido con su cantidad modificada.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderList'

  /order/{idOrder}/{idProd}:
    delete:
      tags:
        - Order
      summary: elimina un producto del pedido.
      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/orderPath'
        - $ref: '#/components/parameters/productPath'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/orderDelete'

      responses:
        200:
          description: muestra un pedido con su cantidad modificada.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/orderList'

  /pay:
    get:
      tags:
        - PaymentMethod
      summary: muestra todos los medios de pagos.

      parameters:
        - $ref: '#/components/parameters/userHeader'

      responses:
        200:
          description: muestra la lista de medios de pagos.
          content:
            application/json:
              schema:
                type: object
                items:
                  $ref: '#/components/schemas/pay'

    post:
      tags:
        - PaymentMethod
      summary: crea un medio de pago.

      parameters:
        - $ref: '#/components/parameters/userHeader'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/payPost'

      responses:
        200:
          description: muestra la lista de medios de pagos.

  /pay/{idPay}:
    put:
      tags:
        - PaymentMethod
      summary: Modifica un medio de pago.

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/payPath'

      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/payPut'

      responses:
        200:
          description: Modifica exitosamente un medios de pagos.
        401:
          description: No está autorizado de realizar esta tarea.

  /pay/method/{idPay}:
    delete:
      tags:
        - PaymentMethod
      summary: Desactiva un medio de pago.

      parameters:
        - $ref: '#/components/parameters/userHeader'
        - $ref: '#/components/parameters/payPath'

      responses:
        200:
          description: Se desactiva un medios de pagos.
        401:
          description: No está autorizado de realizar esta tarea.

components:
  parameters:
    userHeader:
      in: header
      name: id_user
      required: true
      schema:
        type: integer
      description: Manda el ID USER para validaciones.

    userPath:
      in: path
      name: idUser
      required: true
      schema:
        type: integer
      description: Manda el ID USER que desea buscar.

    productPath:
      in: path
      name: idProd
      required: true
      schema:
        type: integer
      description: Manda el ID PRODUCT que desea buscar.

    orderPath:
      in: path
      name: idOrder
      required: true
      schema:
        type: integer
      description: Manda el ID ORDER para validaciones.

    payPath:
      in: path
      name: idPay
      required: true
      schema:
        type: integer
      description: Manda el ID PAY para buscar el medio de pago

  schemas:
    userRegister:
      type: object
      properties:
        username:
          type: string
          example: vero
        fullName:
          type: string
          example: veronica escofe
        email:
          type: string
          example: ver@escofe.com
        phone:
          type: integer
          example: 2964545723
        address:
          type: string
          example: quilmes 1234
        pass:
          type: string
          example: vero123
        repass:
          type: string
          example: vero123

    userLogin:
      type: object
      properties:
        username:
          type: string
          example: vero
        email:
          type: string
          example: ver@escofe.com
        pass:
          type: string
          example: vero123

    userList:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            example: 1
          username:
            type: string
            example: vero
          fullName:
            type: string
            example: veronica escofe
          email:
            type: string
            example: ver@escofe.com
          phone:
            type: integer
            example: 2964545723
          address:
            type: string
            example: quilmes 1646
          pass:
            type: string
            example: vero123
          repass:
            type: string
            example: vero123
          isAdmin:
            type: boolean
            example: true
          isActive:
            type: boolean
            example: false

    productsList:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            example: 1
          nameProduct:
            type: string
            example: empanadas jyq
          description:
            type: string
            example: emapanada de jamon y queso
          price:
            type: integer
            example: 750
          isActive:
            type: boolean
            example: true

    productRegister:
      type: object
      properties:
        nameProduct:
          type: string
          example: empanadas jyq
        description:
          type: string
          example: emapanada de jamon y queso
        price:
          type: integer
          example: 750

    orderList:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            example: 1
          status:
            type: string
            example: en preparacion
          time:
            type: string
            example: 12.35
          numberOrder:
            type: integer
            example: 1
          detailOrder:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                  example: 1
                nameProduct:
                  type: string
                  example: hamburguesa completa
                unit:
                  type: integer
                  example: 2
          total:
            type: integer
            example: 750
          payment_method:
            type: string
            example: efectivo
          id_user:
            type: integer
            example: 1
          user:
            type: string
            example: veronica
          address:
            type: string
            example: quilmes 1646
          isActive:
            type: boolean
            example: true

    orderRegister:
      type: object
      properties:
        payment:
          type: object
          properties:
            user:
              type: string
              example: fer_escofe
            type:
              type: integer
              example: 3
            address:
              type: string
              example: pje sarmiento 930
            close_order:
              type: string
              example: no
        detail:
          type: array
          items:
            type: object
            properties:
              id:
                type: integer
                example: 3
              unit:
                type: integer
                example: 2

    orderPutQty:
      type: object
      properties:
        order:
          type: object
          properties:
            idOrder:
              type: integer
              example: 0
            close_order:
              type: string
              example: si
        detail:
          type: array
          items:
            type: object
            properties:
              id:
                type: integer
                example: 1
              unit:
                type: integer
                example: 5

    orderListHistory:
      type: object
      properties:
        OrderHistory:
          type: array
          items:
            type: object
            properties:
              id:
                type: integer
                example: 0
              nameProduct:
                type: string
                example: empanadas jyq
              unit:
                type: integer
                example: 1

    orderStatus:
      type: object
      properties:
        status:
          type: integer
          example: 5

    orderDelete:
      type: object
      properties:
        close_order:
          type: string
          example: si

    pay:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            example: 5
          method:
            type: string
            example: cheque
          isActive:
            type: boolean
            example: true

    payPost:
      type: object
      properties:
        method:
          type: string
          example: cheque

    payPut:
      type: object
      properties:
        method:
          type: string
          example: cheque
        isActive:
          type: boolean
          example: true
