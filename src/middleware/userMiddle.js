const user = require('../app/models/userModel');

//valida si existe el email
function AuthenticateCreateUser(req, res, next) {
    const email = user.findIndex((users) => users.email === req.body.email);
    if (email != -1) {
        res.status(401).json('registered email');
    } else {
        next();
    }
}

// login de usuario
function AuthenticateLogin(req, res, next) {
    const username = req.body.user;
    const email = req.body.email;
    const pass = req.body.pass;
    const userExist = user.find((users) => users.user === username);
    const emailExist = user.find((users) => users.email === email);
    const passExist = user.find((users) => users.pass === pass);

    if ((userExist && passExist) || (emailExist && passExist)) {
        next();
    } else {
        res.status(401).json({ error: 'user,email or pass does not exist.' });
    }
}

//valida pass
function AuthenticatePassword(req, res, next) {
    const pass = req.body.pass,
        repass = req.body.repass;
    if (pass === repass) {
        next();
    } else {
        res.status(400).json('the password does not match');
    }
}

//valida usuario admin
function AuthenticateAdmin(req, res, next) {
    const idUser = parseInt(req.headers.id_user);
    const admin = user.find((users) => users.id === idUser);

    if (admin.isAdmin === true) {
        next();
    } else {
        res.status(401).json('Username is not Admin');
    }
}

function AuthenticateUser(req, res, next) {
    const idUser = parseInt(req.headers.id_user);
    const userExist = user.find((users) => users.id === idUser);
    if (userExist) {
        next();
    } else {
        res.status(401).json({ error: 'user does not exist.' });
    }
}

module.exports = {
    AuthenticateCreateUser,
    AuthenticateLogin,
    AuthenticatePassword,
    AuthenticateAdmin,
    AuthenticateUser,
};