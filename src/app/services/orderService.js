const orderModel = require('../models/orderModel');
const productModel = require('../models/productModel');
const payModel = require('../models/paymentStatusModel');
const pay = payModel.paymentMethod;
const status = payModel.status;
const serviceProduct = require('../services/productService');
const serviceUser = require('../services/userService');
const servicePay = require('../services/paymentMethodService');

//buscar orden solo el admin
function SearchOrder(idx) {
    const idOrder = parseInt(idx);
    const id = orderModel.find((orders) => orders.id === idOrder);
    if (id) {
        return id.id;
    }
}

//crea la orden
function CreateOrder(data) {
    const body = data;
    const product = body.detail;
    const otherUser = serviceUser.SearchUSer(body.payment);
    const idPay = servicePay.SearchpaymentMethod(body.payment.type);

    const otherOrder = {};

    otherOrder.id = orderModel[orderModel.length - 1].id + 1;

    if (body.payment.close_order === '' || body.payment.close_order === 'no') {
        const id = status.findIndex((type) => type.id === 1);
        otherOrder.status = status[id].status;
    }

    if (body.payment.close_order === 'si') {
        const id = status.findIndex((type) => type.id === 2);
        otherOrder.status = status[id].status;
    }

    otherOrder.time = fechaHoy;
    otherOrder.numberOrder = orderModel[orderModel.length - 1].numberOrder + 1;
    otherOrder.detailOrder = [];
    otherOrder.total = TotalPay(product, otherOrder.id);

    if (idPay != null) {
        otherOrder.payment_method = pay[idPay].method;
    } else {
        otherOrder.payment_method = idPay;
    }
    otherOrder.id_user = otherUser.id;
    otherOrder.user = body.payment.user;

    if (body.payment.address == '') {
        otherOrder.address = otherUser.address;
    } else {
        otherOrder.address = body.payment.address;
    }

    otherOrder.isActive = true;

    product.forEach((idProd) => {
        const product = productModel.find((prod) => prod.id === idProd.id);
        otherOrder.detailOrder.push({
            id: product.id,
            nameProduct: product.nameProduct,
            unit: idProd.unit,
        });
    });

    orderModel.push(otherOrder);
    return otherOrder;
}

//cargar un producto nuevo o cambiar la cantidad
function PutOrderProduct(body, idx) {
    const order = body.order;
    const product = body.detail;
    const id = idx;
    const OtherDetail = {};

    if (order.close_order === 'si') {
        orderModel[id].status = 'confirmado';
    }

    product.forEach((idProd) => {
        const prodId = orderModel[id].detailOrder.find(
            (prod) => prod.id === idProd.id
        );

        if (prodId) {
            orderModel[id].detailOrder[prodId.id].unit = idProd.unit;
        } else {
            const product = productModel.find((prod) => prod.id === idProd.id);
            OtherDetail.id = product.id;
            OtherDetail.nameProduct = product.nameProduct;
            OtherDetail.unit = idProd.unit;
            orderModel[id].detailOrder.push(OtherDetail);
        }
    });
    orderModel[id].total = TotalPay(orderModel[id].detailOrder);
    return orderModel[id];
}

//elimina un producto del pedido
function DeleteProd(order, prod, body) {
    const idOrder = parseInt(order);
    const idProd = parseInt(prod);
    const close_order = body.close_order;

    if (close_order === 'si') {
        orderModel[idOrder].status = 'confirmado';
    }

    const product = orderModel[idOrder].detailOrder.findIndex(
        (prod) => prod.id === idProd
    );

    if (product != -1) {
        orderModel[idOrder].detailOrder.splice(product, 1);
    }
    orderModel[idOrder].total = TotalPay(orderModel[idOrder].detailOrder);
    return orderModel[idOrder];
}

// calcula la cantidad x el precio del producto
function TotalPay(product, idOrder) {
    const detailProduct = product;
    const id = idOrder;
    let total = 0;
    let subtotal = 0;

    detailProduct.forEach((idProd) => {
        const product = serviceProduct.SearchProduct(idProd.id);
        subtotal = parseInt(idProd.unit * product.price);
        total = total + subtotal;
    });
    return total;
}

module.exports = {
    SearchOrder,
    CreateOrder,
    PutOrderProduct,
    DeleteProd,
    TotalPay,
};