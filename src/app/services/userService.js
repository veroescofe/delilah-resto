const userModel = require('../models/userModel');

//busca el usuario x id, username, email
function SearchUSer(data) {
    const userData = data;
    const idUser = parseInt(data);

    if (idUser) {
        const idExist = userModel.find((users) => users.id === idUser);
        return idExist;
    }
    if (userData) {
        const userExist = userModel.find((users) => users.user === userData.user);
        const emailExist = userModel.find(
            (users) => users.email === userData.email
        );
        if (userExist) {
            return userExist;
        } else {
            return emailExist;
        }
    }
}

module.exports = { SearchUSer };