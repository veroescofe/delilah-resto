const paymentModel = require('../models/paymentStatusModel');
const pay = paymentModel.paymentMethod;

//busca rl medio de pago
function SearchpaymentMethod(id) {
    const idPay = parseInt(id);
    const idx = pay.findIndex((pays) => pays.id === idPay);

    if (idx != -1) {
        return idx;
    } else {
        return null;
    }
}

module.exports = {
    SearchpaymentMethod,
};