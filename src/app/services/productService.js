const productModel = require('../models/productModel');

//busca el producto
function SearchProduct(id) {
    const idProduct = parseInt(id);
    const product = productModel.find((product) => product.id === idProduct);
    if (product) {
        return product;
    }
}
module.exports = { SearchProduct };