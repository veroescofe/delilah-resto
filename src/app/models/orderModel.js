const moment = require('moment');

const dateFormat = 'LT a';
fechaHoy = moment().locale('es').format(dateFormat);

const order = [{
    id: 0,
    status: 'pendiente',
    time: fechaHoy,
    numberOrder: 0,
    detailOrder: [
        { id: 4, nameProduct: 'empanadas jyq', unit: 1 },
        { id: 2, nameProduct: 'pizza completa', unit: 2 },
        { id: 1, nameProduct: 'lomito completa', unit: 3 },
    ],
    total: 6050,
    payment_method: 'Efectivo',
    id_user: 0,
    user: 'vero_escofe',
    address: 'indios quilmes 1646',
    isActive: true,
}, ];

module.exports = order;