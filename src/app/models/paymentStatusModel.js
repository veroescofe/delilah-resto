const paymentMethod = [{
    id: 1,
    method: 'efectivo',
    isActive: true,
}, ];

const status = [{
        id: 1,
        status: 'pendiente',
    },
    {
        id: 2,
        status: 'confirmado',
    },
    {
        id: 3,
        status: 'en preparacion',
    },
    {
        id: 4,
        status: 'enviado',
    },
    {
        id: 5,
        status: 'entregado',
    },
];

module.exports = { paymentMethod, status };