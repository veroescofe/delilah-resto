const express = require('express');
const routeUser = express.Router();
const middle = require('../../middleware/userMiddle');
const control = require('../controllers/userController');

//Create user
routeUser.post(
    '/register',
    middle.AuthenticateCreateUser,
    middle.AuthenticatePassword,
    control.Register
);

//Login user
routeUser.post('/login', middle.AuthenticateLogin, control.Login);

//all users
routeUser.get('/user', middle.AuthenticateAdmin, control.GetUser);

//all users
routeUser.get('/user/:idUser', middle.AuthenticateAdmin, control.GetUserId);

//delete user logic
routeUser.delete(
    '/user/:idUser',
    middle.AuthenticateAdmin,
    control.DeleteUserId
);

module.exports = routeUser;