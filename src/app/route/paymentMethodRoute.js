const express = require('express');
const routepayMethod = express.Router();
const middle = require('../../middleware/userMiddle');
const control = require('../controllers/paymentMethodController');

routepayMethod.get('/', middle.AuthenticateUser, control.GetPay);
routepayMethod.post('/', middle.AuthenticateAdmin, control.PostPay);
routepayMethod.put('/:idpay', middle.AuthenticateAdmin, control.PutPay);
routepayMethod.delete(
    '/method/:idpay',
    middle.AuthenticateAdmin,
    control.DeletePay
);
module.exports = routepayMethod;