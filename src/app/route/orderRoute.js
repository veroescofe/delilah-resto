const express = require('express');
const routeOrder = express.Router();
const middle = require('../../middleware/userMiddle');
const control = require('../controllers/orderController');

routeOrder.get('/', middle.AuthenticateAdmin, control.GetOrder);
routeOrder.post('/', middle.AuthenticateUser, control.PostOrders);
routeOrder.put('/', middle.AuthenticateUser, control.PutOrderProduct);
routeOrder.get('/history', middle.AuthenticateUser, control.GetOrderUser);
routeOrder.put('/:idOrder', middle.AuthenticateAdmin, control.PutOrder);
routeOrder.delete(
    '/:idOrder/:idProd',
    middle.AuthenticateUser,
    control.DeleteProd
);

module.exports = routeOrder;