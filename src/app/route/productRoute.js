const express = require('express');
const routeProduct = express.Router();
const middle = require('../../middleware/userMiddle');
const control = require('../controllers/productController');

routeProduct.get('/', middle.AuthenticateUser, control.GetProduct);
routeProduct.post('/', middle.AuthenticateAdmin, control.PostProduct);
routeProduct.get('/:idProd', middle.AuthenticateAdmin, control.GetProducId);
routeProduct.put('/:idProd', middle.AuthenticateAdmin, control.PutProduct);
routeProduct.delete(
    '/:idProd',
    middle.AuthenticateAdmin,
    control.DeleteProduct
);

module.exports = routeProduct;