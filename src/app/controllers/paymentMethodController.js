const paymentMethodModel = require('../models/paymentStatusModel');
const pay = paymentMethodModel.paymentMethod;
const servicie = require('../services/paymentMethodService');

//obtiene todos los metodos de pago
function GetPay(req, res) {
    res.status(200).json({ payMethod: pay });
}

//crea un metodo de pago
function PostPay(req, res) {
    const body = req.body;
    const otherPayMethod = {};
    otherPayMethod.id = pay[pay.length - 1].id + 1;
    otherPayMethod.method = body.method;
    otherPayMethod.isActive = true;
    pay.push(otherPayMethod);

    if (otherPayMethod) {
        res.status(200).json({ PaymennCreate: otherPayMethod });
    }
}

//modificar metodo de pago
function PutPay(req, res) {
    const idx = req.params.idpay;
    const body = req.body;
    const id = servicie.SearchpaymentMethod(idx);

    if (id != null) {
        pay[id].method = body.method;
        pay[id].isActive = body.isActive;
        res.status(200).json({ UpdatePayment: pay[id] });
    } else {
        res.status(200).json({ Menssage: 'Payment does not exist' });
    }
}

//no elimina, cambia el estado a desactivado
function DeletePay(req, res) {
    const idx = req.params.idpay;
    const id = servicie.SearchpaymentMethod(idx);

    if (id != null) {
        pay[id].isActive = false;
        res.status(200).json({ DisablePayment: pay[id] });
    } else {
        res.status(200).json({ Menssage: 'Payment does not exist' });
    }
}

module.exports = { GetPay, PostPay, PutPay, DeletePay };