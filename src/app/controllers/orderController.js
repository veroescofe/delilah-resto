const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');
const serviceOrder = require('../services/orderService');
const payModel = require('../models/paymentStatusModel');
const status = payModel.status;

const moment = require('moment');

const dateFormat = 'LT a';
fechaHoy = moment().locale('es').format(dateFormat);

//obtener todos los pedidos, solo admin
function GetOrder(req, res) {
    res.status(200).json({ order: orderModel });
}

//historial de compras de un usuario, busca x email
function GetOrderUser(req, res) {
    const userData = parseInt(req.headers.id_user);
    const idExist = userModel.find((users) => users.id === userData);

    if (idExist) {
        const orderHistory = [];
        orderModel.forEach(function(order) {
            orderHistory.push(order.detailOrder);
        });
        res.status(200).json({ OrderHistory: orderHistory });
    }
}

//crea un pedido,pasa todos los productos juntos
function PostOrders(req, res) {
    const body = req.body;
    const order = serviceOrder.CreateOrder(body);
    if (order) {
        res.status(200).json({ OrderCreate: order });
    }
}

//modifica el estado de la orden, solo el admin
function PutOrder(req, res) {
    const idParamsOrder = req.params.idOrder;
    const body = req.body;
    const id = serviceOrder.SearchOrder(idParamsOrder);
    const idx = status.findIndex((type) => type.id === body.status);

    if (idx != -1) {
        orderModel[id].status = status[idx].status;
        res.status(200).json({ UpdateStatusOrder: orderModel[id] });
    }

    res.status(200).json({ UpdateStatusOrder: orderModel[id] });
}

//modifica la cantidad del producto en la orden o agrega un nuevo producto
function PutOrderProduct(req, res) {
    const body = req.body;
    const id = serviceOrder.SearchOrder(body.order.idOrder);
    if (orderModel[id].status === 'confirmado') {
        res.status(200).json({
            Menssage: 'closed order',
        });
    } else {
        const order = serviceOrder.PutOrderProduct(body, id);
        res.status(200).json({ Order: order });
    }
    res.status(200).json({ Menssage: 'Error!' });
}

//elimina un producto del detalle de pedido
function DeleteProd(req, res) {
    const idOrder = req.params.idOrder;
    const idProd = req.params.idProd;
    const body = req.body;

    const id = serviceOrder.SearchOrder(idOrder);

    if (
        orderModel[id].status === 'confirmado' ||
        orderModel[id].status === 'en preparacion'
    ) {
        res.status(200).json({
            Mensagge: 'Closed order',
        });
    } else {
        const order = serviceOrder.DeleteProd(idOrder, idProd, body);
        res.status(200).json({ Order: order });
    }
    res.status(200).json({ Menssage: 'Error!' });
}

module.exports = {
    GetOrder,
    GetOrderUser,
    PutOrder,
    PutOrderProduct,
    PostOrders,
    DeleteProd,
};