const userModel = require('../models/userModel');
const productModel = require('../models/productModel');
const servicie = require('../services/productService');

//obtiene todos los productos si es admin sino solo los activos.
function GetProduct(req, res) {
    const idUser = parseInt(req.headers.id_user);
    const userExist = userModel.find((users) => users.id === idUser);

    if (userExist.isAdmin === true) {
        res.status(200).json({ product: productModel });
    } else {
        const productActive = [];
        productModel.forEach(function(prod) {
            if (prod.isActive === true) {
                productActive.push(prod);
            }
        });
        res.status(200).json({ Product: productActive });
    }
}

//muestra un producto solo si es admin
function GetProducId(req, res) {
    const product = servicie.SearchProduct(req.params.idProduct);
    res.status(200).json({ product: productModel[product.id] });
}

//crea un producto el admin
function PostProduct(req, res) {
    const body = req.body;
    const otherProduct = {};
    otherProduct.id = productModel[productModel.length - 1].id + 1;
    otherProduct.nameProduct = body.nameProduct;
    otherProduct.description = body.description;
    otherProduct.price = body.price;
    otherProduct.isActive = true;
    productModel.push(otherProduct);

    if (otherProduct) {
        res.status(200).json({ ProductCreate: otherProduct });
    }
}

//actualiza un producto el admin
function PutProduct(req, res) {
    const idx = req.params.idProd;
    const body = req.body;
    const product = servicie.SearchProduct(idx);
    console.log(idx);
    productModel[product.id].nameProduct = body.nameProduct;
    productModel[product.id].description = body.description;
    productModel[product.id].price = body.price;
    productModel[product.id].isActive = body.isActive;

    res.status(200).json({ UpdateProduct: productModel[product.id] });
}

//no elimina un producto, solo lo desactiva para no mostrarlo al usuario común
function DeleteProduct(req, res) {
    const idx = req.params.idProd;
    const product = servicie.SearchProduct(idx);
    productModel[product.id].isActive = false;

    res.status(200).json({ DisableProduct: productModel[product.id] });
}

module.exports = {
    GetProduct,
    GetProducId,
    PostProduct,
    PutProduct,
    DeleteProduct,
};