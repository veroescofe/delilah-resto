const userModel = require('../models/userModel');
const serviceUser = require('../services/userService');

const moment = require('moment');

const dateFormat = 'LT a';
fechaHoy = moment().locale('es').format(dateFormat);

//registra un usuario
function Register(req, res) {
    const body = req.body;
    const otherUser = {};
    otherUser.id = userModel[userModel.length - 1].id + 1;
    otherUser.user = body.user;
    otherUser.fullName = body.fullName;
    otherUser.email = body.email;
    otherUser.phone = body.phone;
    otherUser.address = body.address;
    otherUser.pass = body.pass;
    otherUser.repass = body.repass;
    otherUser.isAdmin = false;
    otherUser.isActive = true;
    userModel.push(otherUser);

    if (otherUser) {
        res.status(200).json({ UserCreate: otherUser });
    }
}

//login usuario, verifica su estado, si esta inactivo lo habilita para realizar ordenes
function Login(req, res) {
    const body = req.body;
    const user = serviceUser.SearchUSer(body);
    if (user.isActive === false) {
        userModel[user.id].isActive = true;
        res.status(200).json({ Menssage: 'Sesion Started' });
    }
    if (user.isActive === true) {
        res.status(200).json({ Menssage: 'Sesion Started' });
    }
    res.status(401).json({ Menssage: 'error' });
}

//muestra todos los usuarios
function GetUser(req, res) {
    res.status(200).json({ Users: userModel });
}

//muestra usuario por id
function GetUserId(req, res) {
    const idx = req.params.idUser;
    const user = serviceUser.SearchUSer(idx);
    if (user) {
        res.status(200).json({ Users: userModel[user.id] });
    } else {
        res.status(200).json({ Menssage: 'User does not exist' });
    }
}

//no elimina usuario,cambia estado a inactivo
function DeleteUserId(req, res) {
    const idx = req.params.idUser;
    const user = serviceUser.SearchUSer(idx);
    if (user) {
        userModel[user.id].isActive = false;
        res.status(200).json({ DisableUser: userModel[user.id] });
    } else {
        res.status(200).json({ Menssage: 'User does not exist' });
    }
}

module.exports = { Register, Login, GetUser, GetUserId, DeleteUserId };