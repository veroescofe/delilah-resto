const config = require('./config');
const express = require('express');
const server = express();
const cors = require('cors');

server.use(cors());
server.use(express.json());

const user = require('./route/userRoute');
server.use('/', user);

const product = require('./route/productRoute');
server.use('/product', product);

const order = require('./route/orderRoute');
server.use('/order', order);

const paymentMethod = require('./route/paymentMethodRoute');
server.use('/pay', paymentMethod);

server.listen(config.port, () => {
    console.log(`Server started on port ${config.port}`);
});