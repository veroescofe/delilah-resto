**"Delilah Restó", App de pedidos de comida**

El objetivo del proyecto es generar un app de pedidos para un restaurant, la misma es una API escalable, deberá tener base de datos y documentación.

**Tecnologías utilizadas**

**_En el primer sprint_**

- NodeJS
- Express
- Swagger para documentación de API

**Documentación de la API**

Deberá abrir y copiar el archivo **_swagger.yaml_**, esté contiene los endpoints para hacer uso de la API.

**Instalación e inicialización del proyecto**

**_1 - Clonar proyecto_**

Desde la consola deberá clonar el proyecto del repositorio de GitLab de la siguiente manera:

**_git clone https://gitlab.com/veroescofe/delilah-resto.git_**

**_2 - Instalación de dependencias_**

Una vez clonado el proyecto y dentro del mismo utilizando: **_npm install_** , se instalaran todas las dependencias necesarias para el funcionamiento de la API.

**_3 - Inicialización_**

Iniciar desde el archivo: **_/src/app_** , utilizando **_npm run start_**
